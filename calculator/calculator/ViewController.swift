//
//  ViewController.swift
//  calculator
//
//  Created by Danil Nikiforov on 06.10.15.
//  Copyright © 2015 thumbtack. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var display: UILabel!
    var userInTheMiddleOfTypingANumber = false

    @IBAction func appendDigit(sender: UIButton) {
        let digit = sender.currentTitle!
        if userInTheMiddleOfTypingANumber {
            display.text = display.text! + digit
        }else{
            display.text = digit
            userInTheMiddleOfTypingANumber = true
        }
    }

    @IBAction func operate(sender: UIButton) {
        let operation = sender.currentTitle!
        if userInTheMiddleOfTypingANumber{
            enter()
        }
        
        switch operation{
        case "×":performOperation { $0 * $1 }
        case "/":performOperation { $0 / $1 }
        case "+":performOperation { $0 + $1 }
        case "-":performOperation { $0 - $1 }
        default: break
        }
        
    }
    
    func performOperation(operation:(Double, Double)->Double){
        if(operandStack.count>=2) {
            displayValue = operation(operandStack.removeLast(), operandStack.removeLast());
            enter();
            
        }
    }
    
    var operandStack = Array<Double>()

    @IBAction func enter() {
        userInTheMiddleOfTypingANumber = false
        operandStack.append(displayValue)
        print("operandStack: \(operandStack)")
    }
    
    var displayValue: Double {
        get{
            return NSNumberFormatter().numberFromString(display.text!)!.doubleValue
        }
        set{
            display.text = "\(newValue)"
            userInTheMiddleOfTypingANumber = false
        }
    }
}

